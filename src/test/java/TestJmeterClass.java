import configs.*;
import groovy.util.logging.Slf4j;
import models.*;
import org.apache.commons.io.FileUtils;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.config.gui.ArgumentsPanel;
import org.apache.jmeter.control.LoopController;
import org.apache.jmeter.control.gui.TestPlanGui;
import org.apache.jmeter.engine.StandardJMeterEngine;
import org.apache.jmeter.protocol.http.control.HeaderManager;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerProxy;
import org.apache.jmeter.protocol.ssh.sampler.SSHSFTPSampler;
import org.apache.jmeter.report.config.ConfigurationException;
import org.apache.jmeter.report.dashboard.GenerationException;
import org.apache.jmeter.report.dashboard.ReportGenerator;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.testelement.TestPlan;
import org.apache.jmeter.threads.ThreadGroup;
import org.apache.jmeter.threads.gui.ThreadGroupGui;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jmeter.visualizers.SummaryReport;
import org.apache.jorphan.collections.HashTree;
import org.apache.jorphan.collections.ListedHashTree;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import java.io.*;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@EnableConfigurationProperties(value = ServerConfig.class)
@ContextConfiguration(classes = {ServerConfig.class, TestJmeterClass.class, GenerateData.class, KeyCloakApi.class,
ConfigureSamplers.class, ConfigureControllers.class, ConfigureConfigElements.class, GenerateReport.class, ConfigureAssertions.class})
@TestPropertySource(locations = "classpath:application-${env}.properties")
@Slf4j
public class TestJmeterClass {


        @Autowired
        private GenerateReport generateReport;

        @Autowired
        private ConfigureAssertions configureAssertions;

        @Autowired
        private ConfigureControllers configureControllers;

        @Autowired
        private ConfigureSamplers configureSamplers;

        @Autowired
        private ConfigureConfigElements configureConfigElements;

        @Autowired
        private GenerateData generateData;


        File jmeterProperties = new File("jmeter_home/bin/jmeter.properties");
        String repDir = "";

        private static Logger logger = LoggerFactory.getLogger(TestJmeterClass.class);

        private void setJMeterUtil()
        {
                JMeterUtils.setJMeterHome("jmeter_home");
                JMeterUtils.loadJMeterProperties(jmeterProperties.getPath());
                JMeterUtils.initLocale();
                // Set directory for HTML report
                repDir = "report-output";
                JMeterUtils.setProperty("jmeter.reportgenerator.exporter.html.property.output_dir", repDir);
        }


        private ThreadGroup configureThreadGroup(int setDuration, int numberOfThread, int rampUpPeriod, LoopController loopController)
        {
                ThreadGroup threadGroup = new ThreadGroup();
                threadGroup.setName("Example Thread Group");
                threadGroup.setScheduler(true);
                threadGroup.setDuration(setDuration);
                threadGroup.setNumThreads(numberOfThread);
                threadGroup.setRampUp(rampUpPeriod);
                threadGroup.setSamplerController(loopController);
                threadGroup.setProperty(TestElement.TEST_CLASS, ThreadGroup.class.getName());
                threadGroup.setProperty(TestElement.GUI_CLASS, ThreadGroupGui.class.getName());

                return threadGroup;
        }

        private TestPlan configureTestPlan()
        {
                TestPlan testPlan = new TestPlan("Create JMeter Script From Java Code");
                testPlan.setProperty(TestElement.TEST_CLASS, TestPlan.class.getName() );
                testPlan.setProperty(TestElement.GUI_CLASS, TestPlanGui.class.getName());
                testPlan.setUserDefinedVariables((Arguments) new ArgumentsPanel().createTestElement());


                return testPlan;
        }

        private ListedHashTree getHTTPSamplerTestTreePlan(String csvFileName, int numberOfThread, int rampUpPeriod, List<HTTPSamplerProxy> samplerList, LoopController loopController,
                                                          int setDuration, HeaderManager manager, String jmeterFileName) throws IOException {

                ListedHashTree testPlanTree = new ListedHashTree();
                //testPlanTree.add(configureTestPlan());
                ArrayList<String> stringArrayList = new ArrayList<>(Arrays.asList("clientId"));
                testPlanTree.add(configureTestPlan(), configureConfigElements.getCSVConfig(csvFileName, stringArrayList));

                HashTree threadGroupHashTree = testPlanTree.add(configureTestPlan(), configureThreadGroup(setDuration,numberOfThread,rampUpPeriod,loopController));

                threadGroupHashTree.add(manager);

                threadGroupHashTree.add(samplerList);

               // threadGroupHashTree.add(configureAssertions.validateStatementSuccess());

                //System.out.println("Our header" + manager.getHeaders());

                // save generated test plan to JMeter's .jmx file format
                //SaveService.saveTree(testPlanTree, new FileOutputStream("/Users/pertuniatladi/IdeaProjects/PureJavaJmeter/jmeter_home/" + jmeterFileName + ".jmx"));;

                return testPlanTree;
        }



        public void runSFTPSenderHTTPSampler(int numberOfThreads, int rampUpPeriod) throws IOException, ConfigurationException, GenerationException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, InterruptedException {


                if (jmeterProperties.exists()) {
                        //JMeter Engine
                        StandardJMeterEngine jmeter = new StandardJMeterEngine();
                        setJMeterUtil();
                        //add Summarizer output to get test progress in stdout like:

                        // Store execution results into a .jtl file
                        File logFile = new File("src/test/resources/testreport/SFTPSender.csv");

                        //dele
                        // te log file if exists
                        if (logFile.exists()) {
                                boolean delete = logFile.delete();
                                System.out.println("Jtl deleted: " + delete);
                        }

                        List<HTTPSamplerProxy> samplersList = new ArrayList<>();
                        //samplersList.add(0, configureSamplers.addPushCustomer("Add SFTPPush Sender"));
                        //samplersList.add(1, configureSamplers.editPushCustomer("Edit SFTPPush Sender"));
                        samplersList.add(0, configureSamplers.getPushCustomer());
                        //samplersList.add(1, readSFTPSampler("AutomatedReadFile"));

                        //HeaderManager headerManager =  invokeHeaderManager();
                        ReportGenerator reportGenerator = new ReportGenerator(logFile.getPath(), configureAssertions.getReport(logFile)); //creating ReportGenerator for creating HTML report
                        ListedHashTree hashTreeReadDrop = getHTTPSamplerTestTreePlan("/SFTPSenderFiles.csv", numberOfThreads,
                                rampUpPeriod, samplersList, configureControllers.getLoopController(), 30, configureConfigElements.JSONHeaderManager(), "SFTPSender");

                        hashTreeReadDrop.add(hashTreeReadDrop.getArray()[0], configureAssertions.getReport(logFile));

                        jmeter.configure(hashTreeReadDrop);
                        jmeter.run();

                        // Report Generator
                        FileUtils.deleteDirectory(new File(repDir));//delete old report
                        reportGenerator.generate();
                        //deleteReportDashboard();
                        generateReport.generateHtml("src/test/resources/testreport/SFTPSender.csv", "SFTPSenderResults");
                        System.out.println("Test completed. See " + "example.csv file for results");
                        System.out.println("JMeter .jmx script is available at " + "example.jmx");

                }
        }

        public void runSFTPSender270HTTPSampler(int numberOfThreads, int rampUpPeriod) throws IOException, ConfigurationException, GenerationException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, InterruptedException {


                if (jmeterProperties.exists()) {
                        //JMeter Engine
                        StandardJMeterEngine jmeter = new StandardJMeterEngine();
                        setJMeterUtil();
                        //add Summarizer output to get test progress in stdout like:

                        // Store execution results into a .jtl file
                        File logFile = new File("src/test/resources/testreport/SFTPSender270.csv");

                        //dele
                        // te log file if exists
                        if (logFile.exists()) {
                                boolean delete = logFile.delete();
                                System.out.println("Jtl deleted: " + delete);
                        }

                        List<HTTPSamplerProxy> samplersList = new ArrayList<>();
                        //samplersList.add(0, configureSamplers.addPushCustomer("Add SFTPPush Sender"));
                        //samplersList.add(1, configureSamplers.editPushCustomer("Edit SFTPPush Sender"));
                        samplersList.add(0, configureSamplers.getPushCustomer270());
                        //samplersList.add(1, readSFTPSampler("AutomatedReadFile"));

                        //HeaderManager headerManager =  invokeHeaderManager();
                        ReportGenerator reportGenerator = new ReportGenerator(logFile.getPath(), configureAssertions.getReport(logFile)); //creating ReportGenerator for creating HTML report
                        ListedHashTree hashTreeReadDrop = getHTTPSamplerTestTreePlan("/SFTPSenderFiles.csv", numberOfThreads,
                                rampUpPeriod, samplersList, configureControllers.getLoopController(), 30, configureConfigElements.JSONHeaderManager(), "SFTPSender270");

                        hashTreeReadDrop.add(hashTreeReadDrop.getArray()[0], configureAssertions.getReport(logFile));

                        jmeter.configure(hashTreeReadDrop);
                        jmeter.run();

                        // Report Generator
                        FileUtils.deleteDirectory(new File(repDir));//delete old report
                        reportGenerator.generate();
                        //deleteReportDashboard();
                        generateReport.generateHtml("src/test/resources/testreport/SFTPSender270.csv", "SFTPSenderResults270");
                        System.out.println("Test completed. See " + "example.csv file for results");
                        System.out.println("JMeter .jmx script is available at " + "example.jmx");

                }
        }

        public void runSFTPSenderSDCHTTPSampler(int numberOfThreads, int rampUpPeriod) throws IOException, ConfigurationException, GenerationException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, InterruptedException {


                if (jmeterProperties.exists()) {
                        //JMeter Engine
                        StandardJMeterEngine jmeter = new StandardJMeterEngine();
                        setJMeterUtil();
                        //add Summarizer output to get test progress in stdout like:

                        // Store execution results into a .jtl file
                        File logFile = new File("src/test/resources/testreport/SFTPSenderSDC.csv");

                        //dele
                        // te log file if exists
                        if (logFile.exists()) {
                                boolean delete = logFile.delete();
                                System.out.println("Jtl deleted: " + delete);
                        }

                        List<HTTPSamplerProxy> samplersList = new ArrayList<>();
                        //samplersList.add(0, configureSamplers.addPushCustomer("Add SFTPPush Sender"));
                        //samplersList.add(1, configureSamplers.editPushCustomer("Edit SFTPPush Sender"));
                        samplersList.add(0, configureSamplers.getPushCustomer270());
                        //samplersList.add(1, readSFTPSampler("AutomatedReadFile"));

                        //HeaderManager headerManager =  invokeHeaderManager();
                        ReportGenerator reportGenerator = new ReportGenerator(logFile.getPath(), configureAssertions.getReport(logFile)); //creating ReportGenerator for creating HTML report
                        ListedHashTree hashTreeReadDrop = getHTTPSamplerTestTreePlan("/SFTPSenderFiles.csv", numberOfThreads,
                                rampUpPeriod, samplersList, configureControllers.getLoopController(), 30, configureConfigElements.JSONHeaderManager(), "SFTPSenderSDC");

                        hashTreeReadDrop.add(hashTreeReadDrop.getArray()[0], configureAssertions.getReport(logFile));

                        jmeter.configure(hashTreeReadDrop);
                        jmeter.run();

                        // Report Generator
                        FileUtils.deleteDirectory(new File(repDir));//delete old report
                        reportGenerator.generate();
                        //deleteReportDashboard();
                        generateReport.generateHtml("src/test/resources/testreport/SFTPSenderSDC.csv", "SFTPSenderResultsSDC");
                        System.out.println("Test completed. See " + "example.csv file for results");
                        System.out.println("JMeter .jmx script is available at " + "example.jmx");

                }
        }

        @Test
        public void sftpSenderLoadBalancerScenarios() throws InterruptedException, GenerationException, NoSuchAlgorithmException, IOException, KeyManagementException, KeyStoreException, ConfigurationException {
                //run 20 sfp sender users on load balancer
                logger.info("run 20 sfp sender users on load balancer");

                System.out.println("Run 20 sftp sender users on load balancer");
                runSFTPSenderHTTPSampler(20,0);

                //run 40 sfp sender users on load balancer
                System.out.println("Run 40 sftp sender users on load balancer");
                runSFTPSenderHTTPSampler(40,0);

                //run 60 sfp sender users on load balancer
                System.out.println("Run 60 sftp sender users on load balancer");
                runSFTPSenderHTTPSampler(60,0);
        }

        @Test
        public void sftpSenderSDCScenarios() throws InterruptedException, GenerationException, NoSuchAlgorithmException, IOException, KeyManagementException, KeyStoreException, ConfigurationException {
                //run 20 sfp sender users on sdc
                System.out.println("Run 20 sfp sender users on sdc");
                runSFTPSenderSDCHTTPSampler(20,0);

                //run 40 sfp sender users on sdc
                System.out.println("Run 40 sfp sender users on sdc");
                runSFTPSenderSDCHTTPSampler(40,0);

                //run 60 sfp sender users on sdc
                System.out.println("Run 60 sfp sender users on sdc");
                runSFTPSenderSDCHTTPSampler(60,0);
        }

        @Test
        public void sftpSender270Scenarios() throws InterruptedException, GenerationException, NoSuchAlgorithmException, IOException, KeyManagementException, KeyStoreException, ConfigurationException {
                //run 20 sfp sender users on 270
                System.out.println("Run 20 sfp sender users on 270");
                runSFTPSender270HTTPSampler(20,0);

                //run 40 sfp sender users on 270
                System.out.println("Run 40 sfp sender users on 270");
                runSFTPSender270HTTPSampler(40,0);

                //run 60 sfp sender users on 270
                System.out.println("Run 60 sfp sender users on 270");
                runSFTPSender270HTTPSampler(60,0);
        }

        public  void deleteReportDashboard()
        {
                File dir = new File("target/dashboard");
                for(File file: dir.listFiles())
                        if (!file.isDirectory())
                                file.delete();

                System.out.println("Test is done.");
        }

        public  void deleteFiles()
        {
                File dir = new File("src/test/resources/statementfiles");
                for(File file: dir.listFiles())
                        if (!file.isDirectory())
                                file.delete();

                System.out.println("Test is done.");
        }

}
