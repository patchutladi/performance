import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

@ContextConfiguration(
        classes = SpringIntegrationTest.Context.class)
@WebAppConfiguration
public class SpringIntegrationTest {


    @Configuration
    @ComponentScan
    @PropertySources({
            @PropertySource(value = "classpath:application.properties"),
            @PropertySource(value = "classpath:application-${env}.properties", ignoreResourceNotFound = true)
    })
    @PropertySource(value = {"classpath:application.properties", "classpath:application-${env}.properties"}, ignoreResourceNotFound = true)
    static class Context {
        // ...
        // your bean definitions
        //

    }
}
