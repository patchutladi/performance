import configs.*;
import groovy.util.logging.Slf4j;
import models.KeyCloakApi;
import models.ServerConfig;
import org.apache.commons.io.FileUtils;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.config.gui.ArgumentsPanel;
import org.apache.jmeter.control.LoopController;
import org.apache.jmeter.control.gui.TestPlanGui;
import org.apache.jmeter.engine.StandardJMeterEngine;
import org.apache.jmeter.protocol.http.control.HeaderManager;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerProxy;
import org.apache.jmeter.protocol.ssh.sampler.SSHSFTPSampler;
import org.apache.jmeter.report.config.ConfigurationException;
import org.apache.jmeter.report.dashboard.GenerationException;
import org.apache.jmeter.report.dashboard.ReportGenerator;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.testelement.TestPlan;
import org.apache.jmeter.threads.ThreadGroup;
import org.apache.jmeter.threads.gui.ThreadGroupGui;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jmeter.visualizers.SummaryReport;
import org.apache.jorphan.collections.HashTree;
import org.apache.jorphan.collections.ListedHashTree;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@EnableConfigurationProperties(value = ServerConfig.class)
@ContextConfiguration(classes = {ServerConfig.class, TestJmeterClass.class, GenerateData.class, KeyCloakApi.class,
        ConfigureSamplers.class, ConfigureControllers.class, ConfigureConfigElements.class, GenerateReport.class, ConfigureAssertions.class})
@TestPropertySource(locations = "classpath:application-${env}.properties")
@Slf4j
public class SFTPSenderRunner {


        @Autowired
        private GenerateReport generateReport;

        @Autowired
        private ConfigureAssertions configureAssertions;

        @Autowired
        private ConfigureControllers configureControllers;

        @Autowired
        private ConfigureSamplers configureSamplers;

        @Autowired
        private ConfigureConfigElements configureConfigElements;

        @Autowired
        private GenerateData generateData;


        File jmeterProperties = new File("jmeter_home/bin/jmeter.properties");
        String repDir = "";

        private void setJMeterUtil()
        {
                JMeterUtils.setJMeterHome("jmeter_home");
                JMeterUtils.loadJMeterProperties(jmeterProperties.getPath());
                JMeterUtils.initLocale();
                // Set directory for HTML report
                repDir = "report-output";
                JMeterUtils.setProperty("jmeter.reportgenerator.exporter.html.property.output_dir", repDir);
        }


        private ThreadGroup configureThreadGroup(int setDuration, int numberOfThread, int rampUpPeriod, LoopController loopController)
        {
                ThreadGroup threadGroup = new ThreadGroup();
                threadGroup.setName("Example Thread Group");
                threadGroup.setScheduler(true);
                threadGroup.setDuration(setDuration);
                threadGroup.setNumThreads(numberOfThread);
                threadGroup.setRampUp(rampUpPeriod);
                threadGroup.setSamplerController(loopController);
                threadGroup.setProperty(TestElement.TEST_CLASS, ThreadGroup.class.getName());
                threadGroup.setProperty(TestElement.GUI_CLASS, ThreadGroupGui.class.getName());

                return threadGroup;
        }

        private TestPlan configureTestPlan()
        {
                TestPlan testPlan = new TestPlan("Create JMeter Script From Java Code");
                testPlan.setProperty(TestElement.TEST_CLASS, TestPlan.class.getName() );
                testPlan.setProperty(TestElement.GUI_CLASS, TestPlanGui.class.getName());
                testPlan.setUserDefinedVariables((Arguments) new ArgumentsPanel().createTestElement());


                return testPlan;
        }

        private ListedHashTree getYafsHTTPSamplerTestTreePlan(String csvFileName, int numberOfThread, int rampUpPeriod, List<HTTPSamplerProxy> samplerList, LoopController loopController,
                                                          int setDuration, HeaderManager manager, String jmeterFileName) throws IOException {

                ListedHashTree testPlanTree = new ListedHashTree();
                //testPlanTree.add(configureTestPlan());
                ArrayList<String> stringArrayList = new ArrayList<>(Arrays.asList("filePath","clientName"));
                testPlanTree.add(configureTestPlan(), configureConfigElements.getCSVConfig(csvFileName, stringArrayList));

                HashTree threadGroupHashTree = testPlanTree.add(configureTestPlan(), configureThreadGroup(setDuration,numberOfThread,rampUpPeriod,loopController));

                //threadGroupHashTree.add(manager);

                threadGroupHashTree.add(samplerList);


                System.out.println("Our header" + manager.getHeaders());

                // save generated test plan to JMeter's .jmx file format
                //SaveService.saveTree(testPlanTree, new FileOutputStream("/Users/pertuniatladi/IdeaProjects/PureJavaJmeter/jmeter_home/" + jmeterFileName + ".jmx"));;

                return testPlanTree;
        }

        private ListedHashTree getHTTPSamplerTestTreePlan(String csvFileName, int numberOfThread, int rampUpPeriod, List<HTTPSamplerProxy> samplerList, LoopController loopController,
                                                          int setDuration, HeaderManager manager, String jmeterFileName) throws IOException {

                ListedHashTree testPlanTree = new ListedHashTree();
                //testPlanTree.add(configureTestPlan());
                ArrayList<String> stringArrayList = new ArrayList<>(Arrays.asList("clientId"));
                testPlanTree.add(configureTestPlan(), configureConfigElements.getCSVConfig(csvFileName, stringArrayList));

                HashTree threadGroupHashTree = testPlanTree.add(configureTestPlan(), configureThreadGroup(setDuration,numberOfThread,rampUpPeriod,loopController));

                threadGroupHashTree.add(manager);

                threadGroupHashTree.add(samplerList);

               // threadGroupHashTree.add(configureAssertions.validateStatementSuccess());

                System.out.println("Our header" + manager.getHeaders());

                // save generated test plan to JMeter's .jmx file format
                //SaveService.saveTree(testPlanTree, new FileOutputStream("/Users/pertuniatladi/IdeaProjects/PureJavaJmeter/jmeter_home/" + jmeterFileName + ".jmx"));;

                return testPlanTree;
        }



        private ListedHashTree validateStatementMonitorTestTreePlan(String csvFileName, int numberOfThread, int rampUpPeriod, List<HTTPSamplerProxy> samplerList, LoopController loopController,
                                                          int setDuration, HeaderManager manager, String jmeterFileName) throws IOException {

                ListedHashTree testPlanTree = new ListedHashTree();
                //testPlanTree.add(configureTestPlan());
                ArrayList<String> stringArrayList = new ArrayList<>(Arrays.asList("filePath","clientName"));
                testPlanTree.add(configureTestPlan(), configureConfigElements.getCSVConfig(csvFileName, stringArrayList));

                HashTree threadGroupHashTree = testPlanTree.add(configureTestPlan(), configureThreadGroup(setDuration,numberOfThread,rampUpPeriod,loopController));

                threadGroupHashTree.add(manager);

                threadGroupHashTree.add(samplerList);

                threadGroupHashTree.add(configureAssertions.validateStatementSuccess());

                System.out.println("Our header" + manager.getHeaders());

                // save generated test plan to JMeter's .jmx file format
                //SaveService.saveTree(testPlanTree, new FileOutputStream("/Users/pertuniatladi/IdeaProjects/PureJavaJmeter/jmeter_home/" + jmeterFileName + ".jmx"));;

                return testPlanTree;
        }



        private ListedHashTree getSSHSamplerTestTreePlan(String csvFileName, int numberOfThread, int rampUpPeriod, List<SSHSFTPSampler> samplerList,
                                               LoopController loopController, int setDuration, String jmeterFileName) throws IOException {

                ListedHashTree testPlanTree = new ListedHashTree();
                //testPlanTree.add(configureTestPlan());
                ArrayList<String> stringArrayList = new ArrayList<>(Arrays.asList("username" , "password"));
                testPlanTree.add(configureTestPlan(), configureConfigElements.getCSVConfig(csvFileName, stringArrayList));
                HashTree threadGroupHashTree = testPlanTree.add(configureTestPlan(), configureThreadGroup(setDuration,numberOfThread,rampUpPeriod, loopController));
                threadGroupHashTree.add(samplerList);

                threadGroupHashTree.add(samplerList);
                //SaveService.saveTree(testPlanTree, new FileOutputStream("/Users/pertuniatladi/IdeaProjects/PureJavaJmeter/jmeter_home/" + jmeterFileName + ".jmx"));

                return testPlanTree;
        }

//        @Test
        public void loadSFTPUsers() throws URISyntaxException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException {
                generateData.generateSFTPServerData();
        }


        //manual run login 1000 Users  ramp up is 2000 and loop controller is 1
        //@Test
        public void runManualUserLoginTest() throws IOException, ConfigurationException, GenerationException, URISyntaxException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {


        if (jmeterProperties.exists()) {
                //JMeter Engine
                StandardJMeterEngine jmeter = new StandardJMeterEngine();
                setJMeterUtil();
                //add Summarizer output to get test progress in stdout like:

                // Store execution results into a .csv file
                File logFile = new File("target/ManualLogin.csv");
                if (logFile.exists()) {
                        boolean delete = logFile.delete();
                        System.out.println("Jtl deleted: " + delete);
                }

                List<SSHSFTPSampler> samplersList = new ArrayList<>();
                samplersList.add(0, configureSamplers.loginSFTPSampler("ManualLogin"));

                ReportGenerator reportGenerator = new ReportGenerator(logFile.getPath(), configureAssertions.getReport(logFile)); //creating ReportGenerator for creating HTML report
                ListedHashTree hashTree = getSSHSamplerTestTreePlan("/200ManualUsers.csv", 1000,
                        2000, samplersList, configureControllers.getLoopController(), 1800, "200ManualUsers");


                hashTree.add(hashTree.getArray()[0],  configureAssertions.getReport(logFile));

                // Run Test Plan
                jmeter.configure(hashTree);

                jmeter.run();

                // Report Generator
                FileUtils.deleteDirectory(new File(repDir));//delete old report
                reportGenerator.generate();
                generateReport.generateHtml("target/ManualLogin.csv", "SFTPServerManualLogin");

                System.out.println("Test completed. See " + "example.csv file for results");
                System.out.println("JMeter .jmx script is available at " + "example.jmx");

                }
        }


        //manual run drop and read file 100 Users ramp up is 800 and loop controller is infinite with 1800 duration
        //@Test
        public void runManualUserReadDropTest() throws IOException, ConfigurationException, GenerationException {
                if (jmeterProperties.exists()) {
                        //JMeter Engine
                        StandardJMeterEngine jmeter = new StandardJMeterEngine();
                        setJMeterUtil();
                        //add Summarizer output to get test progress in stdout like:

                        // Store execution results into a .jtl file
                        File logFile = new File("target/ManualDropRead.csv");
                        //delete log file if exists
                        if (logFile.exists()) {
                                boolean delete = logFile.delete();
                                System.out.println("Jtl deleted: " + delete);
                        }
                        List<SSHSFTPSampler> samplersList = new ArrayList<>();
                        samplersList.add(0, configureSamplers.dropSFTPSampler("ManualDropFile"));
                        samplersList.add(1, configureSamplers.readSFTPSampler("ManualReadFile"));

                        ReportGenerator reportGenerator = new ReportGenerator(logFile.getPath(), configureAssertions.getReport(logFile)); //creating ReportGenerator for creating HTML report
                        ListedHashTree hashTreeReadDrop = getSSHSamplerTestTreePlan("/10ManualReadDrop.csv", 100,
                                800, samplersList, configureControllers.getLoopController(), 1800, "10ManualReadDrop");

                        hashTreeReadDrop.add(hashTreeReadDrop.getArray()[0], configureAssertions.getReport(logFile));
                        // Run Test Plan

                        jmeter.configure(hashTreeReadDrop);
                        jmeter.run();

                        // Report Generator
                        FileUtils.deleteDirectory(new File(repDir));//delete old report
                        reportGenerator.generate();
                        generateReport.generateHtml("target/ManualDropRead.csv", "SFTPServerManualFileDrop");

                        System.out.println("Test completed. See " + "example.csv file for results");
                        System.out.println("JMeter .jmx script is available at " + "example.jmx");

                }
        }


        //automated run login 80 Users  ramp up is 0 and loop controller is infinite with 1800 duration
//        @Test
        public void runAutomatedUserLoginTest() throws IOException, ConfigurationException, GenerationException {
                if (jmeterProperties.exists()) {
                        //JMeter Engine
                        StandardJMeterEngine jmeter = new StandardJMeterEngine();
                        setJMeterUtil();

                        File logFile = new File("target/runAutomatedLogin.csv");
                        //delete log file if exists
                        if (logFile.exists()) {
                                boolean delete = logFile.delete();
                                System.out.println("Jtl deleted: " + delete);
                        }
                        List<SSHSFTPSampler> samplersList = new ArrayList<>();
                        samplersList.add(0, configureSamplers.loginSFTPSampler("AutomatedLogin"));

                        ReportGenerator reportGenerator = new ReportGenerator(logFile.getPath(), configureAssertions.getReport(logFile)); //creating ReportGenerator for creating HTML report

                        SummaryReport summaryReport = new SummaryReport();


                        ListedHashTree hashTreeReadDrop = getSSHSamplerTestTreePlan("/10AutomatedUsers.csv",20 ,1, samplersList,
                               configureControllers.infinteLoop(), 30, "10AutomatedUsers");

                        hashTreeReadDrop.add(hashTreeReadDrop.getArray()[0], configureAssertions.getReport(logFile));
                        jmeter.configure(hashTreeReadDrop);
                        jmeter.run();

                        // Report Generator
                        FileUtils.deleteDirectory(new File(repDir));//delete old report
                        reportGenerator.generate();

                        generateReport.generateHtml("target/runAutomatedLogin.csv", "SFTPServerAutomatedLogin");
                        System.out.println("Test completed. See " + "example.csv file for results");
                        System.out.println("JMeter .jmx script is available at " + "example.jmx");

                }
        }


        //automated run user drop read file is 10 Users ramp up is 0 and loop controller is infinite with 1800 duration
        //@Test
        public void runAutomatedUserDropReadTest() throws IOException, ConfigurationException, GenerationException {
                if (jmeterProperties.exists()) {
                        //JMeter Engine
                        StandardJMeterEngine jmeter = new StandardJMeterEngine();
                        setJMeterUtil();
                        //add Summarizer output to get test progress in stdout like:

                        // Store execution results into a .jtl file
                        File logFile = new File("target/runAutomatedDropRead.csv");
                        //delete log file if exists
                        if (logFile.exists()) {
                                boolean delete = logFile.delete();
                                System.out.println("Jtl deleted: " + delete);
                        }

                        List<SSHSFTPSampler> samplersList = new ArrayList<>();
                        samplersList.add(0, configureSamplers.dropSFTPSampler("AutomatedDropFile"));
                        samplersList.add(1, configureSamplers.readSFTPSampler("AutomatedReadFile"));

                        ReportGenerator reportGenerator = new ReportGenerator(logFile.getPath(), configureAssertions.getReport(logFile)); //creating ReportGenerator for creating HTML report
                        ListedHashTree hashTreeReadDrop = getSSHSamplerTestTreePlan("/10AutomatedReadDrop.csv", 10,
                                1, samplersList, configureControllers.infinteLoop(), 30, "10AutomatedReadDrop");

                        hashTreeReadDrop.add(hashTreeReadDrop.getArray()[0],  configureAssertions.getReport(logFile));
                        // Run Test Plan

                        jmeter.configure(hashTreeReadDrop);
                        jmeter.run();

                        // Report Generator
                        FileUtils.deleteDirectory(new File(repDir));//delete old report
                        reportGenerator.generate();

                        generateReport.generateHtml("target/runAutomatedDropRead.csv", "SFTPServerAutomatedDropFile");
                        System.out.println("Test completed. See " + "example.csv file for results");
                        System.out.println("JMeter .jmx script is available at " + "example.jmx");

                }
        }



        //drop 20 push statements
        //@Test
        public void runYafsHTTPSampler() throws IOException, ConfigurationException, GenerationException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, InterruptedException {

                generateData.getAccountHash();
                if (jmeterProperties.exists()) {
                        //JMeter Engine
                        StandardJMeterEngine jmeter = new StandardJMeterEngine();
                        setJMeterUtil();
                        //add Summarizer output to get test progress in stdout like:

                        // Store execution results into a .jtl file
                        File logFile = new File("src/test/resources/testreport/DropStatementFile.csv");
                        //delete log file if exists
                        if (logFile.exists()) {
                                boolean delete = logFile.delete();
                                System.out.println("Jtl deleted: " + delete);
                        }

                        List<HTTPSamplerProxy> samplersList = new ArrayList<>();
                        samplersList.add(0, configureSamplers.yafsHttpSampler());
                        //samplersList.add(1, readSFTPSampler("AutomatedReadFile"));

                        //HeaderManager headerManager =  invokeHeaderManager();
                        ReportGenerator reportGenerator = new ReportGenerator(logFile.getPath(), configureAssertions.getReport(logFile)); //creating ReportGenerator for creating HTML report
                        ListedHashTree hashTreeReadDrop = getYafsHTTPSamplerTestTreePlan("/YafsData.csv", 40,
                                0, samplersList, configureControllers.getLoopController(), 30, configureConfigElements.invokeHeaderManager(), "DropStatements");

                        hashTreeReadDrop.add(hashTreeReadDrop.getArray()[0],  configureAssertions.getReport(logFile));
                        // Run Test Plan
                        //System.out.println(hashTreeRea);

                        jmeter.configure(hashTreeReadDrop);
                        jmeter.run();

                        // Report Generator
                        FileUtils.deleteDirectory(new File(repDir));//delete old report
                        reportGenerator.generate();
                        deleteFiles();

                        generateReport.generateHtml("src/test/resources/testreport/DropStatementFile.csv", "YafsDropStatements");
                        System.out.println("Test completed. See " + "example.csv file for results");
                        System.out.println("JMeter .jmx script is available at " + "example.jmx");

                }
        }
        //validate a successful delivery of 20 statements
        //@Test
        public void runHTTPStatementMonitorSampler() throws IOException, ConfigurationException, GenerationException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
                if (jmeterProperties.exists()) {
                        //JMeter Engine
                        StandardJMeterEngine jmeter = new StandardJMeterEngine();
                        setJMeterUtil();
                        //add Summarizer output to get test progress in stdout like:

                        // Store execution results into a .jtl file
                        File logFile = new File("src/test/resources/testreport/MonitorStatementsLogFile.csv");
                        //delete log file if exists
                        if (logFile.exists()) {
                                boolean delete = logFile.delete();
                                System.out.println("Jtl deleted: " + delete);
                        }

                        List<HTTPSamplerProxy> samplersList = new ArrayList<>();
                        samplersList.add(0, configureSamplers.statementMonitorHttpSampler());
                        //samplersList.add(1, readSFTPSampler("AutomatedReadFile"));

                        //HeaderManager headerManager =  invokeHeaderManager();

                        ReportGenerator reportGenerator = new ReportGenerator(logFile.getPath(), configureAssertions.getReport(logFile)); //creating ReportGenerator for creating HTML report


                        ListedHashTree hashTreeReadDrop = validateStatementMonitorTestTreePlan("/YafsData.csv", 30,
                                1, samplersList, configureControllers.getLoopController(), 30, configureConfigElements.invokeHeaderManager(), "TrackStatements");

                        hashTreeReadDrop.add(hashTreeReadDrop.getArray()[0], configureAssertions.getReport(logFile));


                        jmeter.configure(hashTreeReadDrop);
                        jmeter.run();


                        //hashTreeReadDrop.list();

                        // Report Generator
                        FileUtils.deleteDirectory(new File(repDir));//delete old report
                        reportGenerator.generate();

                        generateReport.generateHtml("src/test/resources/testreport/MonitorStatementsLogFile.csv", "StatementMonitorTracking");
                        System.out.println("Test completed. See " + "example.csv file for results");
                        System.out.println("JMeter .jmx script is available at " + "example.jmx");

                }
        }

        //add, edit and get sftp sender profiles
        @Test
        public void runSFTPSenderHTTPSampler() throws IOException, ConfigurationException, GenerationException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, InterruptedException {


                if (jmeterProperties.exists()) {
                        //JMeter Engine
                        StandardJMeterEngine jmeter = new StandardJMeterEngine();
                        setJMeterUtil();
                        //add Summarizer output to get test progress in stdout like:

                        // Store execution results into a .jtl file
                        File logFile = new File("src/test/resources/testreport/SFTPSender.csv");

                        //dele
                        // te log file if exists
                        if (logFile.exists()) {
                                boolean delete = logFile.delete();
                                System.out.println("Jtl deleted: " + delete);
                        }

                        List<HTTPSamplerProxy> samplersList = new ArrayList<>();
                        //samplersList.add(0, configureSamplers.addPushCustomer("Add SFTPPush Sender"));
                        //samplersList.add(1, configureSamplers.editPushCustomer("Edit SFTPPush Sender"));
                        samplersList.add(0, configureSamplers.getPushCustomer());
                        //samplersList.add(1, readSFTPSampler("AutomatedReadFile"));

                        //HeaderManager headerManager =  invokeHeaderManager();
                        ReportGenerator reportGenerator = new ReportGenerator(logFile.getPath(), configureAssertions.getReport(logFile)); //creating ReportGenerator for creating HTML report
                        ListedHashTree hashTreeReadDrop = getHTTPSamplerTestTreePlan("/SFTPSenderFiles.csv", 1000,
                                0, samplersList, configureControllers.getLoopController(), 30, configureConfigElements.JSONHeaderManager(), "SFTPSender");

                        hashTreeReadDrop.add(hashTreeReadDrop.getArray()[0], configureAssertions.getReport(logFile));

                        jmeter.configure(hashTreeReadDrop);
                        jmeter.run();

                        // Report Generator
                        FileUtils.deleteDirectory(new File(repDir));//delete old report
                        reportGenerator.generate();
                        //deleteReportDashboard();
                        generateReport.generateHtml("src/test/resources/testreport/SFTPSender.csv", "SFTPSenderResults");
                        System.out.println("Test completed. See " + "example.csv file for results");
                        System.out.println("JMeter .jmx script is available at " + "example.jmx");

                }
        }



        public  void deleteReportDashboard()
        {
                File dir = new File("target/dashboard");
                for(File file: dir.listFiles())
                        if (!file.isDirectory())
                                file.delete();

                System.out.println("Test is done.");
        }

        public  void deleteFiles()
        {
                File dir = new File("src/test/resources/statementfiles");
                for(File file: dir.listFiles())
                        if (!file.isDirectory())
                                file.delete();

                System.out.println("Test is done.");
        }

}
