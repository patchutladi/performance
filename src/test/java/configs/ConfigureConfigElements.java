package configs;

import models.KeyCloakApi;
import models.ServerConfig;
import org.apache.jmeter.config.CSVDataSet;
import org.apache.jmeter.protocol.http.control.Header;
import org.apache.jmeter.protocol.http.control.HeaderManager;
import org.apache.jmeter.protocol.http.gui.HeaderPanel;
import org.apache.jmeter.testbeans.gui.TestBeanGUI;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.beans.factory.annotation.Autowired;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class ConfigureConfigElements {

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private KeyCloakApi keyCloakApi;

    public CSVDataSet getCSVConfig(String csvFile, ArrayList<String> variables)
    {
        CSVDataSet csvDataSet = new CSVDataSet();
        csvDataSet.setProperty("filename", serverConfig.getCsvPath() + csvFile);
        csvDataSet.setProperty("delimiter", ",");
        csvDataSet.setProperty("recycle", true);
        csvDataSet.setIgnoreFirstLine(true);
        csvDataSet.setQuotedData(true);
        StringBuilder stringBuilder = new StringBuilder();
        for(int j = 0; j <= variables.size() - 1 ;  j++) {
            stringBuilder.append(variables.get(j));
            if (!(j == variables.size() - 1)) {
                stringBuilder.append(",");
            } else {
                break;
            }

        }
        csvDataSet.setProperty("variableNames", stringBuilder.toString());
        csvDataSet.setName("CSV Data");
        csvDataSet.setProperty("shareMode", "shareMode.all");
        csvDataSet.setProperty(TestElement.TEST_CLASS, CSVDataSet.class.getName());
        csvDataSet.setProperty(TestElement.GUI_CLASS, TestBeanGUI.class.getName());
        csvDataSet.setRecycle(true);
        return csvDataSet;
    }


    public HeaderManager invokeHeaderManager() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        keyCloakApi.invokeGetBearerToken();

        HeaderManager headerManager = new HeaderManager();
        headerManager.add(new Header("Authorization",  keyCloakApi.getBearerToken()));
        headerManager.add(new Header("Content-Type", "application/json"));
        headerManager.setProperty(TestElement.TEST_CLASS, HeaderManager.class.getName());
        headerManager.setProperty(TestElement.GUI_CLASS, HeaderPanel.class.getName());
        headerManager.setEnabled(true);

        return headerManager;
    }
    public HeaderManager JSONHeaderManager() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        keyCloakApi.invokeGetBearerToken();

        HeaderManager headerManager = new HeaderManager();
        //headerManager.add(new Header("Authorization",  keyCloakSearch.getBearerToken()));
        headerManager.add(new Header("Content-Type", "application/json"));
        headerManager.setProperty(TestElement.TEST_CLASS, HeaderManager.class.getName());
        headerManager.setProperty(TestElement.GUI_CLASS, HeaderPanel.class.getName());
        headerManager.setEnabled(true);

        return headerManager;
    }

}
