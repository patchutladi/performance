package configs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.SFTPSenderModel;
import models.ServerConfig;
import models.StatementsSearch;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerProxy;
import org.apache.jmeter.protocol.http.util.HTTPFileArgs;
import org.apache.jmeter.protocol.ssh.sampler.SSHSFTPSampler;
import org.springframework.beans.factory.annotation.Autowired;

public class ConfigureSamplers {

    @Autowired
    private ServerConfig serverConfig;

    //invoke statement monitor to search for a client using the client name
    public HTTPSamplerProxy statementMonitorHttpSampler() throws JsonProcessingException {

        StatementsSearch statementsSearchObject = new StatementsSearch("${clientName}");
        ObjectMapper objectMapper = new ObjectMapper();
        HTTPSamplerProxy httpSamplerProxy =  new HTTPSamplerProxy();
        httpSamplerProxy.setEnabled(true);
        httpSamplerProxy.setDomain(serverConfig.getStatementMonitorRoute());
        httpSamplerProxy.setProtocol("https");
        httpSamplerProxy.setPath("statements/search");
        httpSamplerProxy.setMethod("POST");
        httpSamplerProxy.setFollowRedirects(true);
        httpSamplerProxy.setUseKeepAlive(true);
        httpSamplerProxy.setDoMultipart(false);
        httpSamplerProxy.addNonEncodedArgument("", objectMapper.writeValueAsString(statementsSearchObject),"");


        return httpSamplerProxy;
    }

    //invoke sftp sender to add a sftp sender client
    public HTTPSamplerProxy addPushCustomer(String testName) throws JsonProcessingException {
        SFTPSenderModel senderModel = new SFTPSenderModel();
        ObjectMapper objectMapper = new ObjectMapper();
        HTTPSamplerProxy httpSamplerProxy =  new HTTPSamplerProxy();
        httpSamplerProxy.setEnabled(true);
        httpSamplerProxy.setDomain(serverConfig.getSftpSenderRoute());
        httpSamplerProxy.setProtocol("https");
        httpSamplerProxy.setPath("customer");
        httpSamplerProxy.setMethod("POST");
        httpSamplerProxy.setFollowRedirects(true);
        httpSamplerProxy.setUseKeepAlive(true);
        httpSamplerProxy.setDoMultipart(false);
        httpSamplerProxy.addNonEncodedArgument("", objectMapper.writeValueAsString(senderModel), "=");
        return httpSamplerProxy;
    }

    //invoke sftp sender to update a sftp sender client
    public HTTPSamplerProxy editPushCustomer(String testName) throws JsonProcessingException {
        SFTPSenderModel senderModel = new SFTPSenderModel();
        ObjectMapper objectMapper = new ObjectMapper();
        HTTPSamplerProxy httpSamplerProxy =  new HTTPSamplerProxy();
        httpSamplerProxy.setEnabled(true);
        httpSamplerProxy.setDomain(serverConfig.getSftpSenderRoute());
        httpSamplerProxy.setProtocol("https");
        httpSamplerProxy.setPath("customer");
        httpSamplerProxy.setMethod("PUT");
        httpSamplerProxy.setFollowRedirects(true);
        httpSamplerProxy.setUseKeepAlive(true);
        httpSamplerProxy.setDoMultipart(false);
        httpSamplerProxy.addNonEncodedArgument("", objectMapper.writeValueAsString(senderModel), "=");
        return httpSamplerProxy;
    }

    //invoke sftp sender to get a sftp sender client
    public HTTPSamplerProxy getPushCustomer() throws JsonProcessingException {
        HTTPSamplerProxy httpSamplerProxy =  new HTTPSamplerProxy();
        httpSamplerProxy.setEnabled(true);
        httpSamplerProxy.setDomain(serverConfig.getSftpSenderRoute());
        httpSamplerProxy.setProtocol("https");
        httpSamplerProxy.setPath("customer/${clientId}");
        httpSamplerProxy.setMethod("GET");
        httpSamplerProxy.setFollowRedirects(true);
        httpSamplerProxy.setUseKeepAlive(true);
        httpSamplerProxy.setDoMultipart(false);
        return httpSamplerProxy;
    }

    public HTTPSamplerProxy getPushCustomerSDC() throws JsonProcessingException {
        HTTPSamplerProxy httpSamplerProxy =  new HTTPSamplerProxy();
        httpSamplerProxy.setEnabled(true);
        httpSamplerProxy.setDomain(serverConfig.getSftpSenderRoute());
        httpSamplerProxy.setProtocol("https");
        httpSamplerProxy.setPath("customer/${clientId}");
        httpSamplerProxy.setMethod("GET");
        httpSamplerProxy.setFollowRedirects(true);
        httpSamplerProxy.setUseKeepAlive(true);
        httpSamplerProxy.setDoMultipart(false);
        return httpSamplerProxy;
    }
    public HTTPSamplerProxy getPushCustomer270() throws JsonProcessingException {
        HTTPSamplerProxy httpSamplerProxy =  new HTTPSamplerProxy();
        httpSamplerProxy.setEnabled(true);
        httpSamplerProxy.setDomain(serverConfig.getSftpSenderRoute());
        httpSamplerProxy.setProtocol("https");
        httpSamplerProxy.setPath("customer/${clientId}");
        httpSamplerProxy.setMethod("GET");
        httpSamplerProxy.setFollowRedirects(true);
        httpSamplerProxy.setUseKeepAlive(true);
        httpSamplerProxy.setDoMultipart(false);
        return httpSamplerProxy;
    }

    private HTTPSamplerProxy sendKeycloackRequest()
    {
        HTTPSamplerProxy httpSamplerProxy =  new HTTPSamplerProxy();
        httpSamplerProxy.setDomain(serverConfig.getKeycloakUrl());
        httpSamplerProxy.setProtocol("https");
        httpSamplerProxy.setPath("auth/realms/celine/protocol/openid-connect/token");
        httpSamplerProxy.setMethod("POST");
        httpSamplerProxy.setFollowRedirects(true);
        httpSamplerProxy.setUseKeepAlive(true);
        httpSamplerProxy.setDoMultipart(false);
        httpSamplerProxy.addNonEncodedArgument("grant_type","password","=");
        httpSamplerProxy.addNonEncodedArgument("username","abmakerhost","=");
        httpSamplerProxy.addNonEncodedArgument("password","makerPass","=");
        httpSamplerProxy.addNonEncodedArgument("client_id","bison","=");
        httpSamplerProxy.setEnabled(true);
        return httpSamplerProxy;

    }

    //invoke yafs to send a files to a client sftp client
    public HTTPSamplerProxy yafsHttpSampler()
    {
        HTTPSamplerProxy httpSamplerProxy =  new HTTPSamplerProxy();
        HTTPFileArgs httpFileArgs = new HTTPFileArgs();
        httpFileArgs.addHTTPFileArg( "${filePath}", "file","multipart/form-data");
        httpSamplerProxy.setHTTPFiles(httpFileArgs.asArray());
        httpSamplerProxy.setDomain(serverConfig.getYafsRoute());
        httpSamplerProxy.setProtocol("https");
        httpSamplerProxy.setPath("/putmessage/ibm/incomingstatements");
        httpSamplerProxy.setMethod("POST");
        httpSamplerProxy.setFollowRedirects(true);
        httpSamplerProxy.setAutoRedirects(true);
        httpSamplerProxy.setDoMultipart(true);

        return httpSamplerProxy;
    }

    //invoke bank sftp server drop a payment file into the channel
    public SSHSFTPSampler dropSFTPSampler(String testName)
    {
        SSHSFTPSampler sshsftpSampler =  new SSHSFTPSampler();
        sshsftpSampler.setProperty("testname", testName);
        sshsftpSampler.setProperty("hostname",serverConfig.getHostname());
        sshsftpSampler.setProperty("port",serverConfig.getPort());
        sshsftpSampler.setProperty("username","${username}");
        sshsftpSampler.setProperty("password","${password}");
        sshsftpSampler.setProperty("sshkeyfile",serverConfig.getSshkeyPath());
        sshsftpSampler.setProperty("action","put");
        sshsftpSampler.setProperty("source",serverConfig.getPaymentFile());
        sshsftpSampler.setProperty("destination", "/PAYMENTS");
        return sshsftpSampler;
    }

    //invoke bank sftp server read a payment file
    public SSHSFTPSampler readSFTPSampler(String testName)
    {
        SSHSFTPSampler sshsftpSampler =  new SSHSFTPSampler();
        sshsftpSampler.setProperty("testname", testName);
        sshsftpSampler.setProperty("hostname",serverConfig.getHostname());
        sshsftpSampler.setProperty("port",serverConfig.getPort());
        sshsftpSampler.setProperty("username","${username}");
        sshsftpSampler.setProperty("password","${password}");
        sshsftpSampler.setProperty("sshkeyfile",serverConfig.getSshkeyPath());
        sshsftpSampler.setProperty("action","get");
        sshsftpSampler.setProperty("source","/PAYMENTS/PaymentTestingFile.xml");
        return sshsftpSampler;
    }


    //invoke bank sftp server read a payment file
    public SSHSFTPSampler loginSFTPSampler(String testName)
    {
        SSHSFTPSampler sshsftpSampler =  new SSHSFTPSampler();
        sshsftpSampler.setProperty("testname", testName);
        sshsftpSampler.setProperty("hostname",serverConfig.getHostname());
        sshsftpSampler.setProperty("port",serverConfig.getPort());
        sshsftpSampler.setProperty("username","${username}");
        sshsftpSampler.setProperty("password","${password}");
        sshsftpSampler.setProperty("sshkeyfile",serverConfig.getSshkeyPath());
        sshsftpSampler.setProperty("action","ls");
        sshsftpSampler.setProperty("source","/");
        return sshsftpSampler;
    }

}
