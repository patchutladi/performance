package configs;

import models.KeyCloakApi;
import models.ServerConfig;
import org.apache.jmeter.assertions.JSONPathAssertion;
import org.apache.jmeter.assertions.ResponseAssertion;
import org.apache.jmeter.extractor.RegexExtractor;
import org.apache.jmeter.report.config.ConfigurationException;
import org.apache.jmeter.reporters.ResultCollector;
import org.apache.jmeter.reporters.Summariser;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.util.JMeterUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;

public class ConfigureAssertions {

    @Autowired
    private ServerConfig serverConfig;

    @Autowired
    private KeyCloakApi keyCloakApi;

    private RegexExtractor findTheRegexExtractor()
    {
        RegexExtractor regexExtractor = new RegexExtractor();
        regexExtractor.setRefName("token");
        regexExtractor.setRegex("$.access_token");
        regexExtractor.setProperty("Sample.scope","variable");
        return regexExtractor;
    }
    public ResponseAssertion assertKeycloak()
    {
        ResponseAssertion responseAssertion = new ResponseAssertion();
        responseAssertion.addTestString("access");
        responseAssertion.setTestFieldResponseData();
        responseAssertion.setAssumeSuccess(true);
        responseAssertion.getTestType();
        return responseAssertion;
    }
    public JSONPathAssertion validateStatementSuccess()
    {

        JSONPathAssertion jsonPostProcessor = new JSONPathAssertion();
        jsonPostProcessor.setProperty(TestElement.TEST_CLASS, JSONPathAssertion.class.getName());
        jsonPostProcessor.setProperty(TestElement.GUI_CLASS, JSONPathAssertion.class.getName());
        jsonPostProcessor.setEnabled(true);
        jsonPostProcessor.setJsonPath("$.[0].stageSuccessful");
        jsonPostProcessor.setExpectedValue("true");
        jsonPostProcessor.setJsonValidationBool(true);
        jsonPostProcessor.setExpectNull(false);
        jsonPostProcessor.setInvert( false);
        jsonPostProcessor.setIsRegex(true );

        return jsonPostProcessor;
    }

    private Summariser getSummary()
    {
        Summariser summer = null;
        String summariserName = JMeterUtils.getPropDefault("summariser.name", "summary");
        if (summariserName.length() > 0) {
            summer = new Summariser(summariserName);
        }
        return summer;
    }



    public ResultCollector getReport(File logFile) throws ConfigurationException {
        ResultCollector logger = new ResultCollector(getSummary());

        logger.setFilename(logFile.getPath());
        return logger;
    }



}
