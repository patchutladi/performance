package configs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVWriter;
import models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.*;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import za.co.absa.testauto.onboarding.apiaction.RestSecurity;
import za.co.absa.testauto.onboarding.models.ClassifierResponse;
import za.co.absa.testauto.onboarding.utilities.FileNameHelper;
import za.co.absa.testauto.onboarding.utilities.KeycloakApi;
import za.co.absa.testauto.onboarding.utilities.OnePassApi;
import za.co.absa.testauto.onboarding.utilities.UtilityFunctions;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
public class GenerateData {

    @Autowired
    private ServerConfig serverConfig;

    private  KeycloakApi keycloakApi;

    public GenerateData() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
      keycloakApi  = new KeycloakApi();
    }

    public void loadCSV(File file, int numberofRecords, int start) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, URISyntaxException, IOException {

        FileWriter outputfile = new FileWriter(file);
        CSVWriter writer = new CSVWriter(outputfile, CSVWriter.DEFAULT_SEPARATOR,
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);

        RestTemplate restTemplate = new RestTemplate(RestSecurity.requestFactory());

        URI uri = UriComponentsBuilder
                .fromUri(new URI(serverConfig.getRedisServer()))
                .build()
                .encode()
                .toUri();
        ResponseEntity<SFTPServerModel[]> responseEntity = restTemplate.getForEntity(uri, SFTPServerModel[].class);
        ArrayList<SFTPServerModel> arrayList = new ArrayList<>();
        for(int p=0; p<= 3000; p++)
        {
            arrayList.add(responseEntity.getBody()[p]);
        }

        List<String[]>  data = new ArrayList<>();
        List<String[]>  paymentsData = new ArrayList<>();
        for(int j = start ; j<= start + numberofRecords ; j++) {

            try{
                String username = arrayList.get(j).getUsername();
                String password = arrayList.get(j).getPassword();
                String productType = arrayList.get(j).getUserType();
                String clientName = arrayList.get(j).getClientName();
                //System.out.println(clientName);
                if(!file.getAbsoluteFile().getPath().contains("Drop")) {
                    if (password.equals("Abcde@12345") && productType.contains(Products.STATEMENTS.name())) {
                        System.out.println(clientName);
                        data.add(new String[]{username, password});
                        writer.writeAll(data);
                        data.clear();
                    }
                }
                else
                {
                    if(password.equals("Abcde@12345") && productType.contains(Products.PAYMENTS.name()))
                    {
                        System.out.println(clientName);
                        paymentsData.add(new String[]{username, password});
                        writer.writeAll(paymentsData);
                        paymentsData.clear();
                    }
                }

            } catch (NullPointerException ex) {
                continue;
            }

        }
        writer.close();

    }


    //1) get sftpush clients
    //2) use the id to get account hash
    public void getAccountHash() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException, InterruptedException {

        keycloakApi.setKeycloakTokenUrl(serverConfig.getKeycloakUrl());
        ObjectMapper objectMapper = new ObjectMapper();
        CustomerSearchRequest customerSearchRequest =  new CustomerSearchRequest();
        String body = objectMapper.writeValueAsString(customerSearchRequest);

       ResponseEntity<CustomerSearch[]> customerSearchResponseEntity =  keycloakApi.sentRequestWithToken(HttpMethod.POST,serverConfig.getBackofficeserver() + "customerLandingPage/search",body ,
                keycloakApi.loginToKeycloak(serverConfig.getKeycloakUsername(), serverConfig.getKeycloakPassword()), CustomerSearch[].class);

        File dataofStatementFiles =  new File("src/test/resources/files/YafsData.csv");
        FileWriter outputfile = new FileWriter(dataofStatementFiles);
        CSVWriter writer = new CSVWriter(outputfile, CSVWriter.DEFAULT_SEPARATOR,
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);
        List<String[]> data = new ArrayList<>();
       for(int k = 0 ; k <=  customerSearchResponseEntity.getBody().length-1; k++)
       {

           String fileName = "";

           String clientID = String.valueOf(customerSearchResponseEntity.getBody()[k].getId());
           String clientName = customerSearchResponseEntity.getBody()[k].getClientName();
           String accountHash =  getmyAccountHash(clientID);
           if(!accountHash.isEmpty())
           {
              fileName  = generateFileName(accountHash);
              buildFile(fileName);
               String fileNamePath = serverConfig.getStatementFilesPath() + fileName;
               writeToCSV(writer,fileNamePath,clientName, k,  data);
           }

       }
       writer.close();

    }

    public void writeToCSV(CSVWriter writer, String filePath, String clientName, int counter,  List<String[]> data) throws IOException {


        try {
            data.add(new String[]{filePath,clientName});
            writer.writeAll(data);
            data.clear();
        }
        catch (Exception exception)
        {
            System.out.println(exception.getMessage());
        }

    }

    private void buildFile(String filename)
    {
        try {
            File statementFileName = new File( "src/test/resources/statementfiles/" + filename);
            if (statementFileName.createNewFile()) {
                FileWriter myWriter = new FileWriter(statementFileName);
                myWriter.write("Files in Java might be tricky, but it is fun enough!");
                myWriter.close();
                System.out.println("File created: " + statementFileName.getName());

            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public String getmyAccountHash(String clientId) throws InterruptedException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        boolean isAccountClassified = false;
        String url = serverConfig.getClassfierRoute() + "subscriptions/find-all-by-customer-id?customerId=" + clientId;

        RestTemplate template = new RestTemplate(RestSecurity.requestFactory());

        OnePassApi onePassApi = new OnePassApi();
        onePassApi.setClientId(serverConfig.getOnePassID());
        onePassApi.setClientSecret(serverConfig.getOnePassClientSecret());
        onePassApi.setOnePassRoute(serverConfig.getOnePassRoute());
        onePassApi.setGrantType(serverConfig.getOnePassGrantType());
        String onePassToken = onePassApi.loginToOnePass(serverConfig.getOnePassID(), serverConfig.getOnePassScope());

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(onePassApi.getHeaders(onePassToken));

        String accountHash = "";
        try {

            ResponseEntity<ClassifierResponse[]> response = template.exchange(url, HttpMethod.GET, requestEntity, ClassifierResponse[].class);

            if(response.getBody().length != 0 ) {

                accountHash = response.getBody()[0].accountHash;
            }
            else
            {

            }

        } catch (HttpStatusCodeException ex) {
           // assertCommonMethods.assertFailAndLog(url + " returned  " + ex.getStatusCode().value());
        }
        return accountHash;
    }


    public void generateSFTPServerData() throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, URISyntaxException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        //load data for manual users
        File manualUsersFile =  new File("src/test/resources/files/200ManualUsers.csv");
        File manualDropReadFile =  new File("src/test/resources/files/10ManualReadDrop.csv");

        //load data for automated users
        File automatedUsersFile =  new File("src/test/resources/files/10AutomatedUsers.csv");
        File automatedDropReadFile =  new File("src/test/resources/files/10AutomatedReadDrop.csv");

        loadCSV(manualUsersFile, 1000, 0);
        loadCSV(manualDropReadFile, 600,  0 );


        loadCSV(automatedUsersFile, 100, 1800);
        loadCSV(automatedDropReadFile, 1800, 700);


    }

    public String generateFileName(final String accountHash) {
        FileNameHelper fileNameHelper = new FileNameHelper();
        String fileName = "FCKEN_FG_MT942_" + accountHash + "_" + fileNameHelper.dateFormatter.format(fileNameHelper.today)
                + "T" + fileNameHelper.timeFormatter.format(fileNameHelper.today) + "_"
                + UtilityFunctions.generateRandomChars("abcdefghijklmnopqrstuvwzyz1234567890", 4);
        //log.info("Generated file name : " + fileName);
        return fileName;
    }
}
