package configs;

import org.apache.jmeter.control.LoopController;
import org.apache.jmeter.control.TransactionController;
import org.apache.jmeter.control.gui.LoopControlPanel;
import org.apache.jmeter.control.gui.TransactionControllerGui;
import org.apache.jmeter.sampler.DebugSampler;
import org.apache.jmeter.testelement.TestElement;

public class ConfigureControllers {

    // Loop Controller
    public LoopController getLoopController()
    {
        LoopController loopController = new LoopController();
        loopController.setLoops(1);
        loopController.setFirst(true);
        loopController.setName("Loop controller");
        loopController.setProperty(TestElement.TEST_CLASS, LoopController.class.getName());
        loopController.setProperty(TestElement.GUI_CLASS, LoopControlPanel.class.getName());
        loopController.initialize();
        return loopController;
    }
    // Infinite Loop Controller
    public LoopController infinteLoop()
    {
        // Loop Controller
        LoopController loopController = new LoopController();
        loopController.setLoops(LoopController.INFINITE_LOOP_COUNT);
        loopController.setFirst(true);
        loopController.setName("Loop controller");
        loopController.setProperty(TestElement.TEST_CLASS, LoopController.class.getName());
        loopController.setProperty(TestElement.GUI_CLASS, LoopControlPanel.class.getName());
        loopController.initialize();
        return loopController;
    }

    public DebugSampler debugSampler()
    {
        DebugSampler debugSampler = new DebugSampler();
        debugSampler.isEnabled();
        debugSampler.isDisplayJMeterProperties();

        return debugSampler;
    }

    private TransactionController getTransactionController()
    {
        // Transaction Controller
        TransactionController transactionController = new TransactionController();
        transactionController.setGenerateParentSample(true);
        transactionController.setName("TRANSACTION CONTROLLER");
        transactionController.setProperty(TestElement.TEST_CLASS, TransactionController.class.getName());
        transactionController.setProperty(TestElement.GUI_CLASS, TransactionControllerGui.class.getName());
        return transactionController;
    }

}
