package configs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class GenerateReport {

    public void generateHtml(String resultsFile, String reportName) throws IOException {



        FileWriter writer = new FileWriter("target/"+ reportName+ ".html");
        try  {
        BufferedReader reader = new BufferedReader(new FileReader(resultsFile));


        writer.write("<html>\n" +
                "\t<head>\n" +
                "\t\t<title>PERFORMANCE TEST RESULTS</title>\n" +
                "\t\t<meta charset=\"utf-8\" />\n" +
                "\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\" />\n" +
                "\t\t<link rel=\"stylesheet\" href=\"test-classes/reportingfiles/assets/css/main.css\" />\n" +
                "\t\t<noscript><link rel=\"stylesheet\" href=\"test-classes/reportingfiles/assets/css/noscript.css\" /></noscript>\n" +
                "\t</head>\n" +
                "\t<body class=\"is-preload\">\n" +
                "\n" +
                "\t\t<!-- Wrapper -->\n" +
                "\t\t\t<div id=\"wrapper\">\n" +
                "\n" +
                "\t\t\t\t<!-- Header -->\n" +
                "\t\t\t\t\t<header id=\"header\">\n" +
                "\t\t\t\t\t\t<span><img src=\"test-classes/reportingfiles/images/Logo.png\"></span>\n" +
                "\t\t\t\t\t\t<!-- <div class=\"logo\">\n" +
                "\t\t\t\t\t\t\t\n" +
                "\t\t\t\t\t\t</div> -->\n" +
                "\t\t\t\t\t\t<div class=\"content\">\n" +
                "\t\t\t\t\t\t\t<div class=\"inner\">\n" +
                "\t\t\t\t\t\t\t\t<h1>Performance Test Results</h1>\n" +
                "\t\t\t\t\t\t\t</div>\n" +
                "\t\t\t\t\t\t</div>\n" +
                "\t\t\t\t\t\t<nav>\n" +
                "\t\t\t\t\t\t\t<h4>" + new Date().toString()+ "</h4><table border class=\"styled-table\">\n"
                );


        String currentLine;
        while ((currentLine = reader.readLine()) != null) {
            writer.write("<tbody><tr>");

            for(String field: currentLine.split(","))
                writer.write("<td>" + field + "</td>");

            //writer.write("</tr>\n");
        }

        writer.write("</tbody></table></nav>\n" +
                "\t\t\t\t\t</header>\n" +
                "\n" +
                "\t\t\t\t<!-- Footer -->\n" +
                "\t\t\t\t\t<footer id=\"footer\">\n" +
                "\t\t\t\t\t\t<p class=\"copyright\">&copy; H2H</p>\n" +
                "\t\t\t\t\t</footer>\n" +
                "\n" +
                "\t\t\t</div>\n" +
                "\n" +
                "\t\t<!-- BG -->\n" +
                "\t\t\t<div id=\"bg\"></div>\n" +
                "\n" +
                "\t\t<!-- Scripts -->\n" +
                "\t\t\t<script src=\"test-classes/reportingfiles/assets/js/jquery.min.js\"></script>\n" +
                "\t\t\t<script src=\"test-classes/reportingfiles/assets/js/browser.min.js\"></script>\n" +
                "\t\t\t<script src=\"test-classes/reportingfiles/assets/js/breakpoints.min.js\"></script>\n" +
                "\t\t\t<script src=\"test-classes/reportingfiles/assets/js/util.js\"></script>\n" +
                "\t\t\t<script src=\"test-classes/reportingfiles/assets/js/main.js\"></script>\n" +
                "\n" +
                "\t</body>\n" +
                "</html>");

    } catch (IOException e) {
        e.printStackTrace();
    }
        finally {
            writer.close();
        }
    }
}
