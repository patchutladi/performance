package models;

import lombok.Data;
import models.Embedded;

import java.util.ArrayList;

@Data
public class SFTPServerModel {

    //private Embedded _embedded;
   // private ArrayList<Users> users = new ArrayList<>();
    private int clientId;
    private String clientName;
    private String countryName;
    private String globalCustomerId;
    private String username;
    private String password;
    private ArrayList publicKeys = new ArrayList();
    private ArrayList fingerPrints = new ArrayList();
    private String userType;
    private boolean twoFactorAuth;
    private String statementDirectory;
    private String psrDirectory;
    private String paymentsDirectory;
    private String collectionDirectory;
    private Links _links;
}
