package models;

import java.util.ArrayList;

@lombok.Data
public class RegistrationSearchCriteria {
    private int clientId;
    private String clientName;
    private ArrayList<String> statuses = new ArrayList<>();
    private String colleague;
    private String dateFrom;
    private String dateTo;
    private String serviceType;
}
