package models;

import za.co.absa.testauto.onboarding.backofficemodels.RegistrationServices;

import java.util.ArrayList;

@lombok.Data
public class CustomerSearchRequest {

    private ServicingSearchCriteria servicingSearchCriteria;
    private RegistrationSearchCriteria registrationSearchCriteria;

    public CustomerSearchRequest()
    {

       servicingSearchCriteria =  new ServicingSearchCriteria();
       servicingSearchCriteria.setClientName("push");
       ArrayList<String> statuses =  new ArrayList<>();
       statuses.add("APPROVED");
       servicingSearchCriteria.setStatuses(statuses);
       registrationSearchCriteria = new RegistrationSearchCriteria();
       registrationSearchCriteria.setStatuses(statuses);
    }

}

