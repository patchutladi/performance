package models;

import models.ServerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import za.co.absa.testauto.onboarding.utilities.KeycloakApi;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

@lombok.Data
@Configuration
public class KeyCloakApi {

    @Autowired
    private ServerConfig serverConfig;

    private KeycloakApi keycloakApi;

    private String bearerToken;

    public KeyCloakApi() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        keycloakApi = new KeycloakApi();
    }

    public void invokeGetBearerToken() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
       keycloakApi.setKeycloakTokenUrl(serverConfig.getKeycloakUrl());
       setBearerToken(keycloakApi.loginToKeycloak(serverConfig.getKeycloakUsername(), serverConfig.getKeycloakPassword()));

    }




}
