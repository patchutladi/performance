package models;

import java.util.ArrayList;

@lombok.Data
public class CustomerSearch {

    private int id;
    private String clientName;
    private  String createdBy;
    private String lastModified;
    private ArrayList comments;
    private String serviceType;
    private String status;
    private boolean servicingAction;
    private int registrationId;

    public CustomerSearch()
    {
        setClientName("push");
    }
}
