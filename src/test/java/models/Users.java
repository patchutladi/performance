package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import org.apache.commons.collections.iterators.ArrayListIterator;

import java.util.ArrayList;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder
public class Users {

    private int clientId;
    private String clientName;
    private String countryName;
    private String globalCustomerId;
    private String username;
    private String password;
    private ArrayList publicKeys = new ArrayList();
    private ArrayList fingerPrints = new ArrayList();
    private String userType;
    private boolean twoFactorAuth;
    private String statementDirectory;
    private String psrDirectory;
    private String paymentsDirectory;
    private String collectionDirectory;
    private Links _links;

}
