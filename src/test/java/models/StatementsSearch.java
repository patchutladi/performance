package models;

import lombok.Data;



@Data public class StatementsSearch {

    //private String accountNumber;
    private String clientName;
    private  boolean globalUser;

    public StatementsSearch(String clientName)
    {
        setClientName(clientName);
        //setAccountNumber(accountNumber);
        setGlobalUser(true);
    }
}
