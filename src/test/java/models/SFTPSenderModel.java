package models;

import edu.emory.mathcs.backport.java.util.Arrays;

import java.util.ArrayList;


@lombok.Data
public class SFTPSenderModel {

    private int globalCustomerId;
    private String hostAddress;
    private int portNumber;
    private ArrayList<String> hostFingerprints = new ArrayList<>();
    private String sftpUsername;
    private String sftpPassword;
    private String pushDirectory;
    private String psrDirectory;

    public SFTPSenderModel()
    {
        setGlobalCustomerId(38468);
        setHostAddress("host-sftpserver-sit.dev.aws.dsarena.com");
        setPortNumber(8022);
        ArrayList<String> listoffingerPrints = new ArrayList<>(java.util.Arrays.asList("14:06:77:03:70:ff:d8:cd:69:a1:e3:24:31:71:5f:a4"));
        setHostFingerprints(listoffingerPrints);
        setSftpUsername("hosttest");
        setSftpPassword("9REdVNYUJbGv7dc5K0LMJFuzXB7xCJ9SlJ2JvPScKDs=");
        setPushDirectory("statements");
        setPsrDirectory("/");
    }

}
