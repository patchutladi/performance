package models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import models.Users;

import java.util.ArrayList;
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Embedded {

    private ArrayList<Users> users = new ArrayList<>();
}
