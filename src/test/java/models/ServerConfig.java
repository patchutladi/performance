package models;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@lombok.Data
@Configuration
@ConfigurationProperties(prefix = "server")
public class ServerConfig {

        private String hostname;
        private  String port;
        private String sshkeyPath;
        private String csvPath;
        private String paymentFile;
        private String redisServer;
        private String backofficeserver;
        private String keycloakUsername;
        private String keycloakPassword;
        private String keycloakUrl;
        private String classfierRoute;
        private String statementMonitorRoute;
        private String sftpSenderRoute;
        private String yafsRoute;
        private String statementFilesPath;
        private String onePassID;
        private String onePassClientSecret;
        private String onePassRoute;
        private String onePassGrantType;
        private String onePassScope;
        private String sftpSenderRouteSDC;
        private String sftpSenderRoute270;
}

